package tp.p1.game;

import java.util.Map;

import tp.p1.game.info.PrinterType;
import tp.p1.util.Location;
import tp.p1.util.MyStringUtils;

/**
 * Convierte el juego a una cadena de caracteres con un formato en cuadrícula,
 * La clase no realiza impresiones por pantalla. 
 */
public class FormattedPrinter extends GamePrinter {
	private Game game;
	private String screen[][] = 
			new String[Game.boardDimension.getX()][Game.boardDimension.getY()];

	public FormattedPrinter () {
		super();
		System.out.println("construido Formated Printer");
		this.printingType = PrinterType.BOARDPRINTER;	
	}

	private String basicGameInfo() {
		Map<String, Integer> info = game.getGameInfo();
		String retorno = 
				"Life: " + info.get("playerHP")
				+ "\nTurns: " + info.get("turn")
				+ "\nPoints: " + info.get("points")
				+ "\nRemaining aliens: " + info.get("enemyShipNumber")
				+ "\nShockwave: " + (info.get("shockwave") == 1 ? "YES" : "NO")
				+ "\nSupermissile number: " + info.get("supermissile");
		return retorno;
	}

	private void cleanScreen() {
		String empty = " ";
		for (int i = 0; i < Game.boardDimension.getX(); i++)
			for (int j = 0; j < Game.boardDimension.getY(); j++)
				screen[i][j] = empty ;
	}

	private void drawAt(String symbol, Location position) {
		screen[position.getX()][position.getY()] = symbol;
	}
	
	void generateGameBoard() {
		cleanScreen();
		for (GameObject object : game.getObjects().iter())
			drawAt(object.toString(printingType), object.getPosition());
	}
	

	/**
	 * Convierte el juego pasado como argumento en una cuadrícula imprimible
	 * por consola.
	 * @param game Juego actual
	 * @return Representación del juego en cuadrícula
	 */
	@Override
	public String toString(Game game) {
		this.game = game;
		generateGameBoard();	
		
		int numCols = Game.boardDimension.getY();
		int numRows = Game.boardDimension.getX();
		int cellSize = 10;
		int marginSize = 4;
		String vDelimiter = "|";
		String hDelimiter = "-";
		String space = " ";


		String rowDelimiter = MyStringUtils.repeat(hDelimiter, (numCols * (cellSize + 1)) - 1);
		String margin = MyStringUtils.repeat(space, marginSize);
		String lineDelimiter = String.format("%n%s%s%n", margin + space, rowDelimiter);

		StringBuilder str = new StringBuilder();
		str.append(basicGameInfo());
		str.append(lineDelimiter);

		for (int i = 0; i < numRows; i++) {
			str.append(margin).append(vDelimiter);
			for (int j = 0; j < numCols; j++)
				str.append(MyStringUtils.centre(screen[i][j], cellSize)).append(vDelimiter);
			str.append(lineDelimiter);
		}
		
		cleanScreen();
		return str.toString();
	}
}
