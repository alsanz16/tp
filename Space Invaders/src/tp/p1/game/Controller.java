package tp.p1.game;
import java.util.Scanner;

import tp.p1.exceptions.CommandExecuteException;
import tp.p1.exceptions.CommandParseException;
import tp.p1.game.commands.Command;
import tp.p1.game.commands.CommandGenerator;
import tp.p1.game.commands.UserOption;
import tp.p1.game.info.GameStatus;
import tp.p1.game.info.PrinterType;
import tp.p1.util.GameDifficulty;

/**
 * Clase que controla la entrada y salida de información relacionada con el juego.
 * Se encarga de la gestión de los turnos, de solicitar la entrada al jugador y
 * de imprimir el tablero.
 * 
 * Solo debe existir un Controller al mismo tiempo.
 *
 */
public class Controller {
	private GameDifficulty difficulty;
		
	private long seed;
	
	public Controller (GameDifficulty difficulty) {
		this(difficulty, (int) System.currentTimeMillis());
	}

	/**
	 * @param difficulty Dificultad del juego.
	 * @param seed Semilla del generador pseudoaleatorio. Si está ausente, el valor es aleatorio.
	 */
	public Controller (GameDifficulty difficulty, int seed) {
		this.difficulty = difficulty;
		this.seed = seed;
	} 
	
	private void printGreeting (GameStatus gameStatus) {
		String sep = "*****************************";
		String spaces = "      ";
		String greeting = "\n\n\n" + spaces + sep + "\n"
				+ spaces + "*" + spaces + (gameStatus == GameStatus.PLAYER_WINS ? "PLAYER" : "COMPUTER")
				+ "  WINS" + spaces + "*\n"
				+ spaces + sep + "\n\n\n";
		System.out.print(greeting);
	}
	
	/**
	 * Imprime el mensaje en pantalla
	 * @param message Mensaje a imprimir
	 *  
	 */
	public void printMessage(String message) {
		System.out.println(message);
	}
	 
	/**
	 * Crea un nuevo juego y lo gestiona hasta la finalización.
	 */
	public void run() {
		Game game = new Game(difficulty, seed);
		GamePrinter drawer = PrinterType.BOARDPRINTER.getObject();
		UserOption option = UserOption.HELP;
		GameStatus gameStatus = GameStatus.IN_COURSE;
		Scanner in = new Scanner(System.in);
		
		final String prompt = "Please enter a command: ";
		String[] words;
		Command command = null;
		
		System.out.println(drawer.toString(game));

		do {		
			System.out.print(prompt);
			words = in.nextLine().toLowerCase().trim().split ("\\s+");
			try{ 
				command = CommandGenerator.parseCommand(words);
				if (command.execute(game, this)) gameStatus = game.update();
				System.out.println("\n\n\n" + drawer.toString(game)); 
			} catch (CommandParseException | CommandExecuteException e) {
				if (command != null) System.out.println(
						String.format("Failed action\nCause of exception:",
						command.getName()));
				System.out.println(e.toString() + "\n\n");
			} 
		} while (gameStatus == GameStatus.IN_COURSE);
		
		System.out.println(drawer.toString(game));
		in.close();
		if (option != UserOption.EXIT) printGreeting(gameStatus);
	}

}
