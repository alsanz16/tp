package tp.p1.game;

import tp.p1.game.info.PrinterType;

/**
 * Impresor del juego que devuelve una copia serializada del juego en marcha.
 *
 */
public class Stringifier extends GamePrinter {

	public Stringifier () {
		super();
		printingType = PrinterType.SERIALIZER;	
	}

	/**
	 * Devuelve una copia serializada del juego en marcha, que puede guardarse
	 * en un archivo para cargar desde él.
	 *
	 */
	@Override
	public String toString(Game game) {
		String retorno = "---Space Invaders v2.0---\n\n";
		retorno += String.format("G;%d\nL;%s\n", game.getGameInfo().get("turn"), 
				game.getDifficulty().toString());
		for (GameObject object: game.getObjects().iter())
			retorno += object.toString(printingType) + "\n";
		return retorno;
	}
}
