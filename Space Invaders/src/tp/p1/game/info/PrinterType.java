package tp.p1.game.info;

import tp.p1.game.FormattedPrinter;
import tp.p1.game.Game;
import tp.p1.game.GamePrinter;
import tp.p1.game.Stringifier;

public enum PrinterType {
	BOARDPRINTER("boardprinter",
			"prints the game formatted as a board of dimension: "),
	SERIALIZER("serializer",
			"prints the game as plain text");
	
	private String printerName;
	private String helpText;
	
	private PrinterType(String name, String text) {
		printerName = name;
		helpText = text;
	}
	
	public String getHelp (Game game) {
		String helpString = helpText;
		if (this == PrinterType.BOARDPRINTER) 
			helpString +=  String.format("%sx%s",
					Game.boardDimension.getY(),
					Game.boardDimension.getX());
		return helpString;
		
	}	

	public GamePrinter getObject() {
		return (this == PrinterType.BOARDPRINTER ? new FormattedPrinter()
					: new Stringifier());
	}
}
