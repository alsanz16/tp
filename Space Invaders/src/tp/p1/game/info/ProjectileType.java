package tp.p1.game.info;

public enum ProjectileType {
	MISSILE(1, "oo", MovementDirection.UP, "M"),
	SUPERMISSILE(2, "xx", MovementDirection.UP, "X"),
	PROJECTILE(1, ".", MovementDirection.DOWN, "B"),
	SHOCKWAVE(1, "", null, ""),
	EXPLOSION(1, "", null, "");

	private MovementDirection direction;
	private int dmg;
	private String symbol;
	private String mapSymbol;

	ProjectileType(int dmg, String symbol, MovementDirection direction, String mapSymbol) {
		this.dmg = dmg;
		this.direction = direction;
		this.symbol = symbol;
		this.mapSymbol = mapSymbol;
	}
	
	public MovementDirection getDirection() {
		return direction;
	}

	public int getDmg() {
		return dmg;
	}

	public String getMapSymbol() {
		return mapSymbol;
	}

	public String getSymbol() {
		return symbol;
	}
};