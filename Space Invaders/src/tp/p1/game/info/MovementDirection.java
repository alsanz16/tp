package tp.p1.game.info;

import tp.p1.exceptions.DirectionFormatException;
import tp.p1.util.Location;

/**
 * Clase que recoje las posibles direcciones en el juego.
 *
 */
public enum MovementDirection {
	DOWN(new Location(1, 0)), 
	LEFT(new Location(0, -1)), 
	RIGHT(new Location(0, 1)), 
	UP(new Location(-1, 0));

	/**
	 * Convierte una string dada en una dirección válida o lanza {@link DirectionFormatException}
	 * @param input String representando una dirección
	 * @return La dirección correspondiente a la string (o null si no)
	 * @throws DirectionFormatException Si {@code input} no se corresponde con ninguna dirección válida
	 */
	public static MovementDirection parseDirection(String input) 
			throws DirectionFormatException {
		MovementDirection direction = null;
		for (MovementDirection posibleMove: MovementDirection.values())
			if (input.equalsIgnoreCase(posibleMove.toString()))
				direction = posibleMove;
		if (direction == null) throw new DirectionFormatException("La dirección introducida no es válida");
		return direction;
	}
	
	private int code;
	private Location coordinates;

	/**
	 * @param movement Coordenadas bidimensionales asociadas a la dirección.
	 */
	MovementDirection(Location movement) {
		coordinates = movement;
	}

	public Location getCoordinates() {
		return coordinates;
	}
};