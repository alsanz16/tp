package tp.p1.game;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import tp.p1.exceptions.CommandExecuteException;
import tp.p1.exceptions.FileContentsException;
import tp.p1.exceptions.MissileInFlightException;
import tp.p1.exceptions.NoPointsEnoughException;
import tp.p1.exceptions.NoShockwaveException;
import tp.p1.exceptions.NoSupermissileBought;
import tp.p1.exceptions.OffWorldException;
import tp.p1.game.info.GameStatus;
import tp.p1.game.info.MovementDirection;
import tp.p1.game.info.ShipType;
import tp.p1.naves.BaseShip;
import tp.p1.naves.DestroyerShip;
import tp.p1.naves.RegularShip;
import tp.p1.naves.UCMShip;
import tp.p1.naves.proyectiles.Shockwave;
import tp.p1.util.GameDifficulty;
import tp.p1.util.Location;

/**
 * Juego del Space Invaders. 
 * Toma acciones a traves de los métodos de la interfaz {@link IPlayerController}.
 * El cliente debe encargarse de llamar a {@link Game#update()} cada turno, de 
 * comprobar el estado del juego y de imprimir por pantalla el estado del juego.
 * 
 * Solo debe existir un Game en cualquier momento dado.
 *
 */
public class Game implements IPlayerController{
	public static Location boardDimension = new Location(8, 9);

	private GameDifficulty difficulty;
	private GameStatus gameStatus = GameStatus.IN_COURSE;

	private Random generator;
	private GameObjectList objects;
	
	private UCMShip player;
	private int points;
	private int turn;
	
	/**
	 * @param difficulty La dificultad del juego
	 * @param seed Semilla para el pseudogenerador aleatorio
	 */
	Game(GameDifficulty difficulty, long seed) {
		this.difficulty = difficulty;
		this.generator = new Random(seed);
		initializeGame();
	}
	
	/**
	 * Añade el objeto pasado como argumento a los elementos del juego
	 * @param newObject Objeto a añadir a los elementos del juego
	 */
	public void addObject(GameObject newObject) {
		objects.add(newObject);
	}

	/**
	 * De existir puntos para ello, compra un supermisil. Si no, lanza {@link NoPointsEnoughException}
	 */
	@Override
	public void buySuperMissile() throws CommandExecuteException {
		int superMissileCost = difficulty.getSuperMissileCost();
		boolean exito = false;
		if (points >= superMissileCost) {
			player.addSuperMissile(1);
			points -= superMissileCost;
		}
		else throw new NoPointsEnoughException("Not enough points to buy supermissile");
	}
	
	
	public GameDifficulty getDifficulty() {
		return difficulty;
	}

	
	/**
	 * Devuelve un mapa con información del juego
	 * @return Información del juego: tiene los siguientes campos 
	 *  :: playerHP, turn, points, enemyShipNumber, shockwave, supermissile
	 */
	public Map<String, Integer> getGameInfo() {
		
		Map<String, Integer> infoMap = new HashMap<String, Integer> ();
		infoMap.put("playerHP", player.getHp());
		infoMap.put("turn", turn);
		infoMap.put("points", points);
		infoMap.put("enemyShipNumber", objects.getEnemyShipNumber());
		infoMap.put("shockwave", player.getShockwave() ? 1 :0);
		infoMap.put("supermissile", player.getSuperMissileNumber());
		return infoMap;
	}

	public GameObjectList getObjects() {
		return objects;
	}
	
	private GameObjectList initializeBoard(GameDifficulty difficulty, Game game) {
		GameObjectList board = new GameObjectList(game, new UCMShip(game));
		String map = difficulty.getMap();
		for (int i = 0; i < Game.boardDimension.getX(); i++)
			for (int j = 0; j < Game.boardDimension.getY(); j++) {
				String symbol = "" + map.charAt(i*Game.boardDimension.getY() + j);
				if (symbol.equals(ShipType.REGULAR.getMapSymbol()))
					board.add(new RegularShip (game, new Location (i,j)));
				else if (symbol.equals(ShipType.DESTROYER.getMapSymbol()))
					board.add(new DestroyerShip (game, new Location (i,j)));
			}		
		return board;
	}
	
	 /**
	 * Deja el juego en el estado inicial
	 */
	public void initializeGame() {
		objects = initializeBoard(difficulty, this);
		player = objects.getPlayer();
		initializeVariables();
	}
	 
	 private void initializeVariables() {
		points = 0;
		turn = 0;
		setShockwave(false);
	}	
	
	/**
	 * Devuelve si el objeto pasado como argumento está en el límite de movimiento
	 * del tablero, teniendo en cuenta la dirección del movimiento.
	 * @param ship Objeto a estudiar
	 * @return Está en el límite del tableo
	 */
	public Boolean isAtLimit(GameObject ship) {
		return ship.getPosition().getY() == Game.boardDimension.getY() - 1
				&& objects.getMovementDirection() == MovementDirection.RIGHT
				|| ship.getPosition().getY() == 0 && 
				objects.getMovementDirection() == MovementDirection.LEFT;
				
	}

	/**
	 * Carga un nuevo juego desde el archivo pasado como argumento. De no ser posible,
	 * lanza {@link FileContentsException}.
	 * @param serializedGame Archivo con un juego serializado.
	 */
	public void loadGame(BufferedReader serializedGame) {
		FileVerifier verifier = new FileVerifier();
		GameObject object;
		initializeVariables();
		this.objects = new GameObjectList(this);

		// TODO guardar estado del juego
		try {
			String line;
			line = serializedGame.readLine();
			if (verifier.verifyCycleString(line)) 
				this.turn = Integer.parseInt(line.split(";")[1]);
			
			line = serializedGame.readLine();
			if (verifier.verifyLevelString(line)) 
				this.difficulty = GameDifficulty.parseDifficulty(line.split(";")[1]);
			
			Map <Integer, BaseShip> owners = new HashMap <Integer, BaseShip> ();
			while ((line = serializedGame.readLine()) != null) {
				object = GameObjectGenerator.parse(line, this, verifier, owners);
				if(object != null) addObject(object);
				else throw new FileContentsException("");
				
			this.player = objects.getPlayer();
			}
		} catch(Exception e) {
			throw new FileContentsException("El archivo no es válido");
		};	
	}
		
	/**
	 * Mueve la UCMShip en la dirección pasada como argumento, de no ser posible
	 * se lanza {@link OffWorldException}.
	 * @param direction Dirección a mover
	 */
	public boolean move(MovementDirection direction) 
			throws CommandExecuteException
	{
		if (!player.move(direction))
			throw new OffWorldException("No se pudo realizar el movimiento");
		return true;
	}
	
	/**
	 * Decide aleatoriamente si ocurre un evento con probabilidad {@code probability}
	 * @param probability Probabilidad del evento a estudiar
	 * @return {@code True} si el evento ocurre, {@code False} si no
	 */
	public boolean randomChance(double probability) {
		return generator.nextFloat() < probability;
	}
	
	/**
	 * Suma al jugador la cantidad indicada de puntos
	 * @param quantity Número de puntos a sumar
	 */
	public void receivePoints(int quantity) {
		points += quantity;
	}
	
	/**
	 * Cambia el estado del juego.
	 * @param gameStatus Nuevo estado del juego
	 */
	public void setGameStatus(GameStatus gameStatus) {
		this.gameStatus = gameStatus;
	}	
	
	/**
	 * Cambia el estado de la Shockwave ({@code True} significa disponible)
	 * @param b Nuevo estado de Shockwave.
	 */
	public void setShockwave(boolean b) {
		objects.getPlayer().setShockwave(b);	
	}
	
	
	/**
	 * De estar disponible, añade al juego una {@link Shockwave}. En caso contrario,
	 * lanza {@link NoShockwaveException}.
	 */
	public void shockWave() throws CommandExecuteException {
		if (player.getShockwave()) addObject(new Shockwave(this, player));
		else throw new NoShockwaveException("Cannot realease shockwave: "
				+ "no shockwave available");
	}

	/**
	 * Se intenta añadir al juego un proyectil del tipo indicado por el argumento. Si ya
	 * hay un misil en vuelo, se lanza {@link MissileInFlightException}.
	 * Si se intenta lanzar un supermisil sin haberlo comprado previamente,
	 * se lanza {@link NoSupermissileBought}
	 * @param superMissile Indica si se quiere lanzar un supermisil {@code True} o
	 * un misil estándar
	 * 
	 */
	public void shootLaser(boolean superMissile) throws CommandExecuteException {
		boolean success = false;
		if (superMissile && player.getSuperMissileNumber() > 0
				|| !superMissile)
			success = player.shoot(superMissile);
		else throw new NoSupermissileBought("Cannot fire  "
				+ "supermissile: there is no supermissile");
		if (!success) 
			throw new MissileInFlightException("Cannot fire missile: "
					+ "missile already exists on board");
	}
	
	/**
	 * Lleva a cabo un nuevo turno y devuelve el estado del juego al final de éste.
	 * @return Nuevo estado del juego
	 */
	public GameStatus update() {
		objects.passTurn();
		turn++;
		return gameStatus;
	}
}


