package tp.p1.game;

import java.util.Map;

import tp.p1.game.info.Entity;
import tp.p1.game.info.MovementDirection;
import tp.p1.game.info.PrinterType;
import tp.p1.naves.BaseShip;
import tp.p1.util.GameEventList;
import tp.p1.util.Location;

/**
 * Clase base de todos los objetos del juego.
 *
 */
public abstract class GameObject implements IAttack {
	protected Entity entity;
	protected Game game;
	protected boolean isValid = true;
	protected Location position;

	
	protected GameObject (Game game, Entity entity, Location position) {
		this.game = game;
		this.entity = entity;
		this.position = position.copy();
	}

	/**
	 * Devuelve si el objeto es una nave o un proyectil
	 * @return Naturaleza del objeto
	 */
	public Entity getEntity() {
		return entity;
	}
	
	/** 
	 * @return Posición actual del objeto
	 */
	public Location getPosition () {
		return position;
	}
	
	/**
	 * Devuelve si el objeto está en el límite del mapa. Solo
	 * devuelve resultados significativos para las naves con
	 * movimiento grupal.
	 * @return El objeto está en el límite del mapa
	 */
	public boolean isAtLimit() {
		return false;	
	}
	
	/**
	 * Devuelve si el objeto sigue siendo válido (no ha sido
	 * destruido ni ha salido del mapa)
	 * @return El objeto es válido
	 */
	public boolean isValid() {
		return isValid;
	}
	
	
	/**
	 * Mueve al objeto en la dirección pasada como argumento
	 * @param direction Dirección a mover
	 * @return {@code True} si el movimiento fue posible, {@code False} si no
	 */
	public boolean move(MovementDirection direction) {
		return position.setPosition(direction.getCoordinates());
	}
	
	/**
	 * Crea un objeto desde la cadena de caracteres serializada 
	 * @param stringFromFile Cadena serializada
	 * @param game Juego actual
	 * @param verifier Verificador de validez de cadenas
	 * @param owners Mapa indicando la relación entre naves y sus proyectiles. Las naves
	 * deben añadir su etiqueta al mapa, y los proyectiles comprobar si alguna nave
	 * comparte su etiqueta.
	 * 
	 * Para que funcione bien, es necesario que las naves sean parseadas antes que los
	 * proyectiles.
	 * @return Un objeto válido o {@code null} si la cadena no se corresponde con ninguna entidad.
	 */
	public abstract GameObject parse(String stringFromFile, Game game, FileVerifier verifier, 
			Map<Integer, BaseShip> owners);
	
	/**
	 * Realiza un nuevo turno.
	 * @param timeToMove Es un turno de movimiento de las naves alienígenas, de acuerdo con la dificultad. 
	 */
	public abstract void passTurn(boolean timeToMove);
	
	/**
	 * Devuelve una representación del objeto como cadena de caracteres.
	 * @param printingType Tipo de impresión deseado
	 * @return Cadena de caracteres con el formato especificado por {@code printingType}
	 */
	public abstract String toString(PrinterType printingType);
	
	/**
	 * Permite añadir eventos al final del turno como suma de puntos, avisar de la destrucción del ovni...
	 * La lista de eventos se procesa al final de cada turno.
	 * @param events Lista de eventos
	 * @return Lista de eventos con los eventos propios añadidos.
	 */
	public abstract GameEventList update(GameEventList events);
}
