package tp.p1.game.commands;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import tp.p1.exceptions.CommandParseException;
import tp.p1.exceptions.FileContentsException;
import tp.p1.game.Controller;
import tp.p1.game.Game;

public class LoadCommand extends Command {
	private static UserOption type = UserOption.SAVE;
	public LoadCommand() {
		this("");
		help = "save: saves program status in file";
	}
	
	private LoadCommand(String filename) {
		super(type.toString(), type.getSymbol(), filename);
	}

	@Override
	public boolean execute(Game game, Controller controller) {
		String header = "---Space Invaders v2.0---";
		 try (BufferedReader in = new BufferedReader(new FileReader(details))) {
			 String line = in.readLine();
			 if (!line.contentEquals(header)) 
				 throw new FileContentsException("Incorrect heading");
			 
			 String arr[] = {"save", "backup"};
			 CommandGenerator.parseCommand(arr).execute(game, controller);
			 
			 in.readLine();
			 game.loadGame(in);
			 in.close();
			 controller.printMessage("Game succesfully loaded from file " + details);
		} catch (IOException | FileContentsException e) {
			System.out.println("");
			String arr[] = {"load", "backup"};
			CommandGenerator.parseCommand(arr).execute(game, controller);
			throw new FileContentsException("El archivo no es válido");
		}
		return false;
		
	}
	
	@Override
	public Command parse(String[] commandWords) throws CommandParseException {
		LoadCommand newCommand = null;
		try {
			newCommand = new LoadCommand(commandWords[1] + ".dat");
		} catch (IndexOutOfBoundsException e) {
			new CommandParseException("Es necesario un nombre de archivo");
		}
		return newCommand;
	}
}

