package tp.p1.game.commands;

import tp.p1.exceptions.CommandExecuteException;
import tp.p1.exceptions.CommandParseException;
import tp.p1.game.Controller;
import tp.p1.game.Game;

/**
 * Familia de clases de los comandos
 *
 */
public abstract class Command {
	protected static final String incorrectArgsMsg ="Incorrect argument format";
	protected static final String incorrectNumArgsMsg ="Incorrect number of arguments";
	protected final String details;
	protected String help;
	
	protected final String name;
	protected final String shortcut;
	
	protected Command(String name, String shortcut, String details) {
		this.name = name;
		this.shortcut = shortcut;
		this.details  = details;
	}
	
	/**
	 * Ejecuta el comando dado. En caso de no ser posible lanza una excepción derivada de 
	 * {@link CommandExecuteException}.
	 * @param game Juego en marcha
	 * @param controller Controlador del juego
	 * @return {@code True} si debe pasarse a un nuevo turno tras la ejecución, {@code False} si no
	 * @throws CommandExecuteException Si ha sido imposible completar el comando. En este caso,
	 * no se pasa a un nuevo turno.
	 */
	public abstract boolean execute(Game game, Controller controller) throws CommandExecuteException;
	
	public String getHelp(){
		return help;
	}
	
	public String getName() {
		return name.toLowerCase();
	}
	
	protected boolean matchCommandName(String name) {
		return this.shortcut.equalsIgnoreCase(name) ||
				this.name.equalsIgnoreCase(name);
	}
	
	/**
	 * Lee un comando dado. Si el nombre del comando no se corresponde con el comando
	 * dado, devuelve {@code null}. Si el nombre se corresponde pero los argumentos
	 * son incorrectos, devuelve {@link CommandParseException}
	 * @param commandWords
	 * @return
	 * @throws CommandParseException
	 * @throws NumberFormatException
	 */
	public Command parse(String[] commandWords) throws CommandParseException {
		return matchCommandName(commandWords[0]) ? this : null;
	}
}
