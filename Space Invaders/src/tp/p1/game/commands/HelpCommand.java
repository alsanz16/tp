package tp.p1.game.commands;

import tp.p1.game.Controller;
import tp.p1.game.Game;

/**
 * Muestra la ayuda de todos los comandos.
 * Uso: help
 */
public class HelpCommand extends Command {
	private static UserOption type = UserOption.HELP;

	public HelpCommand() {
		super(type.toString(), type.getSymbol(), "");
		help = "help: Prints this help message";
	}

	@Override
	public boolean execute(Game game, Controller controller) {
		for (Command command: CommandGenerator.getAvailableCommands())
			controller.printMessage(command.getHelp());
		return false;
	}

}
