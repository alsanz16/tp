package tp.p1.game.commands;

import tp.p1.game.Controller;
import tp.p1.game.Game;

public class UpdateCommand extends Command {
	private static UserOption type = UserOption.NONE;
	public UpdateCommand() {
		super(type.toString(), "", "");
		help = "[none]: Skips one cycle";
	}

	@Override
	public boolean execute(Game game, Controller controller) {
		return true;
	}

}
