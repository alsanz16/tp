package tp.p1.game.commands;

import tp.p1.exceptions.CommandParseException;

/**
 * Genera comandos a partir de una cadena de caracteres dada.
 *
 */
public class CommandGenerator {
	private static Command[] availableCommands = {
			new ListCommand(),
			new HelpCommand(),
			new ResetCommand(),
			new ExitCommand(),
			new MoveCommand(),
			new ShockwaveCommand(),
			new ShootCommand(),
			new UpdateCommand(),
			new ComprarCommand(),
			new ListPrintersCommand(),
			new SaveCommand(),
			new LoadCommand()
			};
	
	public static Command[] getAvailableCommands() {
		return availableCommands;
	}

	/**
	 * Dado un conjunto de argumentos, trata de encontrar un comando válido. En caso
	 * de que no sea posible, lanza {@link CommandParseException}.
	 * @param commandWords Vector de argumentos: en la primera posición se encuentra el nombre
	 * del comando, en las siguientes los argumentos.
	 * @return Un comando válido o {@code null} si no ha sido posible.
	 * @throws CommandParseException Si no existe ningún comando asociado a esa cadena
	 */
	public static Command parseCommand (String[] commandWords) 
		throws CommandParseException
	{
		Command commandRead = null;
		for (Command availableCommand: availableCommands)
			commandRead = commandRead == null ? availableCommand.parse(commandWords) : commandRead;
		if (commandRead == null) throw new CommandParseException("No valid command was recognized");
		return commandRead;
	}

}
