package tp.p1.game.commands;

import tp.p1.exceptions.CommandExecuteException;
import tp.p1.exceptions.CommandParseException;
import tp.p1.exceptions.DirectionFormatException;
import tp.p1.game.Controller;
import tp.p1.game.Game;
import tp.p1.game.info.MovementDirection;

public class MoveCommand extends Command {
	private static final int maximumMovement = 2;
	private static UserOption type = UserOption.MOVE;
	private MovementDirection direction;
	int ammount;
	

	public MoveCommand() {
		super(type.toString(), type.getSymbol(), "");
		help = "move <left|right><1|2>: Moves UCM-Ship to the indicated direction.";

	}
	
	private MoveCommand(MovementDirection direction, int ammount) {
		this();
		this.direction = direction;
		this.ammount = ammount;
	}

	@Override
	public boolean execute(Game game, Controller controller) 
			throws CommandExecuteException	{
		for (int i = 0; i < Math.min(ammount, maximumMovement); i++)
			game.move(direction);
		return true;
	}

	@Override
	public Command parse(String[] commandWords) throws CommandParseException  {
		Command newCommand = null;
		if (matchCommandName(commandWords[0])) {
			try {
				MovementDirection direction = MovementDirection.parseDirection(commandWords[1]);
				ammount = Integer.parseInt(commandWords[2]);
				newCommand = new MoveCommand (direction, ammount);
				
			} catch(IndexOutOfBoundsException | NumberFormatException | 
					DirectionFormatException e) { 
				throw new CommandParseException("The command wasn't understood." +
				"Expected format is: move <1|2> <left|right>");
			}
		}
		return newCommand;
	}
}
