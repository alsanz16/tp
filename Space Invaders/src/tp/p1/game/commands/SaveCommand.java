package tp.p1.game.commands;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import tp.p1.exceptions.CommandParseException;
import tp.p1.game.Controller;
import tp.p1.game.Game;
import tp.p1.game.GamePrinter;
import tp.p1.game.info.PrinterType;

public class SaveCommand extends Command {
	private static UserOption type = UserOption.SAVE;
	public SaveCommand() {
		this("");
		help = "save: saves program status in file";
	}
	
	private SaveCommand(String filename) {
		super(type.toString(), type.getSymbol(), filename);
	}

	@Override
	public boolean execute(Game game, Controller controller) {
		GamePrinter printer = PrinterType.SERIALIZER.getObject();
		 try {
			PrintWriter out 
			   = new PrintWriter(new BufferedWriter(new FileWriter(details)));
			out.write(printer.toString(game));
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		controller.printMessage(String.format("Game successfully saved in file %s. "
				+ "Use the load command to reload it", details));
		return false;
		
		
	}
	
	@Override
	public Command parse(String[] commandWords) throws CommandParseException {
		SaveCommand newCommand = null;
		if (matchCommandName(commandWords[0])) {
			try {
				newCommand = new SaveCommand(commandWords[1] + ".dat");
			} catch (IndexOutOfBoundsException e) {
				new CommandParseException("Es necesario un nombre de archivo");
			}
		}
		return newCommand;
	}
}

