package tp.p1.game.commands;

import tp.p1.game.Controller;
import tp.p1.game.Game;
import tp.p1.game.info.ShipType;

/**
 * Muestra una lista con los tipos de nave del juego y sus estadísticas.
 * Uso: list
 */
public class ListCommand extends Command {
	private static UserOption type = UserOption.LIST;
	public ListCommand() {
		super(type.toString(), type.getSymbol(), "");
		help = "list: Prints the list of available ships";
	}

	@Override
	public boolean execute(Game game, Controller controller) {
		for (ShipType ship: ShipType.values()) {
			controller.printMessage(ship.getFullName()
					+ ": Points: " + ship.getAwardedPoints()
					+ ": - Harm: " + (ship.getProjectile() != null ? ship.getProjectile().getDmg() : 0) 
					+ ": - Shield: " + ship.getHp()
			);
		}
		return false;
	}
}
