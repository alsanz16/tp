package tp.p1.game.commands;

import tp.p1.game.Controller;
import tp.p1.game.Game;
import tp.p1.game.info.GameStatus;

/**
 * Sale del juego.
 * Uso: exit
 *
 */
public class ExitCommand extends Command {
	private static UserOption type = UserOption.EXIT;

	public ExitCommand() {
		super(type.toString(), type.getSymbol(), "");
		help = "exit: Terminates the program.";
	}

	@Override
	public boolean execute(Game game, Controller controller) {
		game.setGameStatus(GameStatus.GAME_EXITED);
		return true;
	}

}
