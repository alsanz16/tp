package tp.p1.game.commands;

import tp.p1.exceptions.CommandExecuteException;
import tp.p1.exceptions.CommandParseException;
import tp.p1.game.Controller;
import tp.p1.game.Game;

/**
 * Compra el objeto especificado en el primer argumento.
 * Uso : comprar <supermisil>
 *
 */
public class ComprarCommand extends Command {
	private static UserOption type = UserOption.COMPRAR;
	public ComprarCommand() {
		this("");
	}
	
	private ComprarCommand(String details) {
		super(type.toString(), type.getSymbol(), details);
		help = "comprar <object>: Buys the object from the game";
	}

	@Override
	public boolean execute(Game game, Controller controller) throws CommandExecuteException {
		game.buySuperMissile();
		return false;
	}

	@Override
	public Command parse(String[] commandWords) throws CommandParseException {
		String superMisil = "supermisil";
		Command newCommand = null;
		
		if (matchCommandName(commandWords[0])) {
			try {
				boolean match = commandWords[1].equalsIgnoreCase(superMisil);
				if (!match) throw new CommandParseException("The first argument must be a valid game buyable object");
				newCommand = this;
			} catch(IndexOutOfBoundsException e) { 
				throw new CommandParseException ("The" + type.name() +"command requires one argument");
			}
		}
		return newCommand;
	}
}
