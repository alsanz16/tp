package tp.p1.game.commands;

import tp.p1.exceptions.CommandExecuteException;
import tp.p1.exceptions.CommandParseException;
import tp.p1.game.Controller;
import tp.p1.game.Game;

public class ShootCommand extends Command {
	private static UserOption type = UserOption.SHOOT;
	private boolean supermissile;
	
	public ShootCommand() {
		this("");
	}
	
	public ShootCommand(String details) {
		super(type.toString(), type.getSymbol(), details);
		help = "shoot: UCM-Ship launches a missile";
	}
	
	private ShootCommand(boolean supermissile) {
		this();
		this.supermissile = supermissile;
	}
	
	@Override
	public boolean execute(Game game, Controller controller) 
			throws CommandExecuteException {
		game.shootLaser(supermissile);
		return true;
	}

	@Override
	public Command parse(String[] commandWords) {
		boolean success = matchCommandName(commandWords[0]);
		Command newCommand = null;
		if (success) {
			boolean supermissile = false;
			String supermisil = "supermisil";
			if (commandWords.length == 1 || (supermissile = commandWords[1].equalsIgnoreCase(supermisil)))
				 newCommand = new ShootCommand(supermissile);
			else throw new CommandParseException("Usage: shoot <supermisil>");
		}
		return newCommand;
	}
}
