package tp.p1.game.commands;

import tp.p1.game.Controller;
import tp.p1.game.Game;
import tp.p1.game.info.PrinterType;

/**
 * Muestra una lista con los pobiles impresores del juego y un mensaje de ayuda
 * Uso: listPrinters
 */
public class ListPrintersCommand extends Command{
	private static UserOption type = UserOption.LISTPRINTERS;
	public ListPrintersCommand() {
		super(type.toString(), type.getSymbol(), "");
		help = "listPrinters: Prints the list of possible printers";
	}

	@Override
	public boolean execute(Game game, Controller controller) {
		for (PrinterType printer: PrinterType.values())
			controller.printMessage(printer.toString() + ": " + printer.getHelp(game));
		return true;
	}

}
