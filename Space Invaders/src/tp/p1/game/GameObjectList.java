package tp.p1.game;

import tp.p1.game.info.Entity;
import tp.p1.game.info.MovementDirection;
import tp.p1.naves.UCMShip;
import tp.p1.naves.UFOShip;
import tp.p1.util.GameDifficulty;
import tp.p1.util.GameEvent;
import tp.p1.util.GameEventList;
import tp.p1.util.Location;

/**
 * Lista de objetos del juego. Para que el funcionamiento sea el esperado,
 * el jugador debe añadirse en primer lugar, seguido por las naves y los
 * proyectiles.
 *
 */
public class GameObjectList {
	private static int initialSize = 10;
	private int counter = 0;
	private GameDifficulty difficulty;
	private Game game;
	private boolean limitReached = false;
	private GameObject list[];
	private MovementDirection movementDirection = MovementDirection.LEFT;
	private int size;

	private Boolean ufo = false;
	
	/**
	 * Crea una lista de objetos de juego. El cliente debe añadir al jugador
	 * en primer lugar para que la lista funcione correctamente
	 * @param game Juego en curso
	 */
	public GameObjectList(Game game) {
		list = new GameObject[initialSize];
		size = initialSize;
		this.game = game;
		this.difficulty = game.getDifficulty();
	}

	/**
	 * Crea una lista de objetos de juego, añadiendo al jugador en la primera posición
	 * @param game Juego en marcha
	 * @param player Jugador
	 */
	public GameObjectList(Game game, UCMShip player) {
		this(game);
		add(player);
	}

	/**
	 * Añade el objeto pasado como argumento a la lista de objetos
	 * @param newObject Objeto del juego
	 */
	public void add(GameObject newObject) {
		if (counter == size)
			this.expandList();
		list[counter++] = newObject;
	}

	private Boolean checkLimit() {
		for (GameObject object : this.iter())
			if (object.isAtLimit())
				return true;
		return false;
	}

	private void expandList() {
		GameObject newList[] = new GameObject[size * 2];
		size *= 2;

		for (int i = 0; i < counter; i++)
			newList[i] = list[i];
		list = newList;
	}

	private void generateUFO() {
		if (!ufo && game.randomChance(difficulty.getOvniFrequency())) {
			ufo = true;
			add(new UFOShip(game));
		}
	}
	
	/**
	 * @return Número de naves enemigas en el tablero
	 */
	public int getEnemyShipNumber() {
		int enemies = 0;
		for (int i = 0; i < counter; i++)
			if (list[i].getEntity() == Entity.SHIP) 
				enemies++;
		return enemies-1;
	}
	
	/**
	 * Devuelve la dirección a la que deben moverse las naves con movimiento
	 * grupal este turno.
	 * @return Dirección de movimiento: LEFT, RIGTH o DOWN
	 */
	public MovementDirection getMovementDirection() {
		return limitReached ? MovementDirection.DOWN : movementDirection;
	}

	/**
	 * Devuelve la nave del jugador. Si el jugador no ha sido introducido en primer lugar,
	 * se provocará un error en tiempo de ejecución.
	 * @return Nave del jugador
	 */
	public UCMShip getPlayer() {
		return (UCMShip) list[0];
	}
	
	private boolean isTimeToMove() {
		int turn = game.getGameInfo().get("turn");
		return turn %  difficulty.getMovementSpeed() == 0 ||
				limitReached;
	}
	
	/**
	 * El método permite la iteración de los objetos del juego mediante
	 * el uso de forEach sobre el vector devuelto.
	 * @return Vector iterable con los objetos actualmente en el tablero.
	 */
	public GameObject[] iter() {
		GameObject copyList[] = new GameObject[counter];
		for (int i = 0; i < counter; i++)
			copyList[i] = list[i];
		return copyList;
	}
		
	/**
	 * Realiza las acciones de las naves enemigas correspondientes a un nuevo
	 * turno y procesa los eventos subsecuentes.
	 */
	public void passTurn() {
		limitReached = checkLimit();
		if (limitReached) swapDirection();
		
		for(int i = 0; i < counter; i++)
			list[i].passTurn(isTimeToMove());
		generateUFO();
		processEvents(update());

	}

	private void processEvents(GameEventList events) {
		for (GameEvent event: events.iter())
			GameEvent.processEvent(game, event);
	}
	
	public void setMovementDirection(MovementDirection movementDirection) {
		this.movementDirection = movementDirection;
	}

	public void setUFO(boolean b) {
		ufo = b;
	}

	private void swapDirection() {
		movementDirection = (movementDirection == MovementDirection.LEFT ? 
				MovementDirection.RIGHT :	MovementDirection.LEFT);
	}

	/**
	 * Elimina los elementos no válidos de la lista y recoge los nuevos
	 * eventos generados.
	 * @return Eventos generados en este turno.
	 */
	public GameEventList update() {
		int i = 0;
		GameEventList events = new GameEventList();
		while (i < counter)  {
			list[i].update(events);
			if (!list[i].isValid() && i > 0) list[i] = list[--counter];
			else i++;
		}
		if (getEnemyShipNumber() == 0) events.add(GameEvent.NO_ENEMY_LEFT);
		return events;
	}

}
