package tp.p1.game;

import tp.p1.exceptions.CommandParseException;
import tp.p1.exceptions.DirectionFormatException;
import tp.p1.game.info.MovementDirection;
import tp.p1.game.info.ShipType;
import tp.p1.util.GameDifficulty;
import tp.p1.util.Location;

/**
 * Contiene diversos métodos que verifican el correcto estado de un archivo
 * de carga del juego.
 *
 */
public class FileVerifier {
	public static final String separator1 = ";";
	public static final String separator2 = ",";
	public static final String labelRefSeparator = " - ";
	// for robustness
	public static final String readSeparator1 = "\\s*"+ separator1 + "\\s*"; public static final String readSeparator2 = "\\s*"+ separator2 + "\\s*"; public static final String readLabelRefSeparator = "\\s*"+ "-" + "\\s*";
	private static boolean verifyBool(String boolString) {
		return boolString.equals("true") || boolString.equals("false");
	}
	
	/**
	 * Evalúa si la cadena pasada como argumento representa una coordenada válida
	 * @param x Coordenada x
	 * @param y Coordenada y
	 * @param game Juego actual
	 * @return {@code (x,y)} es una posición del tablero válida
	 */
	private static boolean verifyCoords(int x, int y, Game game) { 
		return new Location(x, y).isValid();
	}

	private static boolean verifyCurrentCycle(int currentCycle) { 
		return currentCycle >= 0;
	}
	
	private static boolean verifyCycleToNextAlienMove(int cycle, GameDifficulty level) { 
		return 0 <= cycle && cycle <= level.getMovementSpeed();
	}
	
	private static boolean verifyDir(MovementDirection dir) { 
		return dir != null;
	}
	
	private static boolean verifyLabel(String label) { 
		return Integer.parseInt(label) > 0;
	}
	
	private static boolean verifyLevel(GameDifficulty level) { 
		return level != null;
	}
	
	// Don’t catch NumberFormatException.
	
	private static boolean verifyLives(int live, int armour) { 
		return 0 < live && live <= armour;
	}
	
	private static boolean verifyPoints(int points) { 
		return points >= 0;
	}
	

	private String foundInFileString = "";
	
	private void appendToFoundInFileString(String linePrefix) { 
		foundInFileString += linePrefix;
	}
	
	
	// Don’t catch NumberFormatException.
	/**
	 * Evalúa si la cadena pasada como argumento representa una nave alien válida
	 * @param lineFromFile cadena a comprobar
	 * @param game Juego actual
	 * @param armour vida máxima de la nave alienígena
	 * @return {@code lineFromFile} es una nave alien válida
	 */
	public boolean verifyAlienShipString(String lineFromFile, Game game, int armour) 
	{ 
		String[] words = lineFromFile.split(readSeparator1); appendToFoundInFileString(words[0]);
		if (words.length != 5) return false;
		String[] coords = words[1].split (readSeparator2);
		try {
			if ( !verifyCoords(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), game)
					|| ! verifyLives(Integer.parseInt(words[2]), armour)
					|| ! verifyLabel(words[3])
					|| MovementDirection.parseDirection(words[4]) == null)
				return false;
		} catch (DirectionFormatException e) {
			return false;
		} 
		return true; 
	}

	/**
	 * Evalúa si la cadena pasada como argumento representa un contador de turno
	 * válido.
	 * @param cycleString Cadena a comprobar
	 * @return {@code cycleString} representa un contador de turno válido
	 */
	public boolean verifyCycleString(String cycleString) { 
		String[] words = cycleString.split (readSeparator1); appendToFoundInFileString(words[0]);
		if (words.length != 2
				|| !verifyCurrentCycle(Integer. parseInt(words[1]))) 
			return false;
		return true; 
		}
	
	/**
	 * Evalúa si la cadena pasada como argumento representa una dificultad
	 * de juego válida.
	 * @param levelString Cadena a comprobar
	 * @return {@code levelString} representa una dificultad de juego válida
	 */
	public boolean verifyLevelString(String levelString) 
	{ 
		String[] words = levelString.split (readSeparator1); 
		appendToFoundInFileString(words[0]);
		try {
			if (words.length != 2
					|| !verifyLevel(GameDifficulty.parseDifficulty(words[1]))) 
				return false;
		} catch (CommandParseException e) {
			return false;
		}
		return true; 
	}
	
	// Don’t catch NumberFormatException.
	/**
	 * Evalúa si la cadena pasada como argumento representa un ovni válido
	 * @param lineFromFile Cadena a comprobar
	 * @param game Juego actual
	 * @param armour Vida máxima de un ovni
	 * @return {@code lineFromFile} representa un ovni válido
	 */
	public boolean verifyOvniString(String lineFromFile, Game game, int armour) { 
		String[] words = lineFromFile.split(readSeparator1); 
		appendToFoundInFileString(words[0]);
		if (words.length != 3) return false;
		String[] coords = words[1].split (readSeparator2);
		if ( !verifyCoords(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), game)
			|| ! verifyLives (Integer.parseInt(words[2]), armour) ) return false;
		return true; 
	}
	
	// Don’t catch NumberFormatException.
	/**
	 * Evalúa si la cadena pasada como argumento representa un jugador válido
	 * @param lineFromFile Cadena a comprobar
	 * @param game Juego actual
	 * @param armour Vida máxima del jugador
	 * @return {@code lineFromFile} es un jugador válido
	 */
	public boolean verifyPlayerString(String lineFromFile, Game game, int armour)
	{ 
		String[] words = lineFromFile.split(readSeparator1); appendToFoundInFileString(words[0]);
		if (words.length != 7) return false;
		String[] coords = words[1].split (readSeparator2);
		if ( !verifyCoords(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), game)
				|| ! verifyLives(Integer.parseInt(words[2]), ShipType.UCM.getHp())
				|| ! verifyLabel(words[3])
				|| ! verifyPoints(Integer.parseInt(words[4]))
				|| ! verifyBool(words[5]) )
			return false; 
		return true;
	}
	

	/**
	 * Evalúa si la cadena pasada como argumento representa un proyectil válido
	 * @param lineFromFile Cadena a comprobar
	 * @param game Juego actual
	 * @return {@code lineFromFile} es un proyectil válido
	 */
	public boolean verifyWeaponString(String lineFromFile, Game game) { 
		String[] words = lineFromFile.split(readSeparator1);
		if (words.length != 3) return false;
		appendToFoundInFileString(words[0]);
		String[] coords = words[1].split (readSeparator2);
		if ( !verifyCoords(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), game) ||
				!verifyLabel(words[2])) return false;
		return true; 
	}
}