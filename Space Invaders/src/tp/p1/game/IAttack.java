package tp.p1.game;

/**
 * Interfaz de reacción a proyectiles. Los objetos que sufran daño
 * de alguno de los proyectiles indicados, deben sobreescribir el
 * método.
 *
 */
public interface IAttack {
	default Boolean performAttack(GameObject other) { return false; }
	default Boolean receiveBombAttack(Integer damage) { return false; }
	default Boolean receiveExplosionAttack(Integer damage) {return true; }
	default Boolean receiveMissileAttack(Integer damage) { return false; }
	default Boolean receiveShockWaveAttack(Integer damage) { return false; }
}
