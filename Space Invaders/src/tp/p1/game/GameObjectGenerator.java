package tp.p1.game;
import java.util.Map;

import tp.p1.naves.BaseShip;
import tp.p1.naves.DestroyerShip;
import tp.p1.naves.RegularShip;
import tp.p1.naves.UCMShip;
import tp.p1.naves.UFOShip;
import tp.p1.naves.proyectiles.Bomb;
import tp.p1.naves.proyectiles.Missile;
import tp.p1.naves.proyectiles.Shockwave;
import tp.p1.naves.proyectiles.SuperMissile;

/**
 * Generador de objetos de juego, prueba todas las opciones para parsear
 * una cadena de carácteres dada hasta encontrar una correcta.
 *
 */
public class GameObjectGenerator {
	

	private static GameObject[] availableGameObjects = { 
		new UCMShip(),
		new UFOShip(),
		new RegularShip(), 
		new DestroyerShip(), 
		new Shockwave(), 
		new Bomb(),
		new Missile(),
		new SuperMissile()
	};
	
	/**
	 * Prueba todas las formas de convertir la cadena de caracteres en un objeto
	 * válido. 
	 * @param stringFromFile Cadena de caracteres
	 * @param game Juego actual
	 * @param verifier Verificador de cadenas de caracteres
	 * @param owners Mapa con asociación etiquetas / naves para restaurar la relación entre
	 * las naves y sus proyectiles.
	 * @return De ser una cadena válida, devuelve el objeto correspondente. Si no, devuelve {@code null}
	 */
	public static GameObject parse(String stringFromFile, Game game, FileVerifier verifier, 
			Map<Integer, BaseShip> owners) { 
		GameObject gameObject = null;
		for (GameObject object: availableGameObjects) { 
			gameObject = object.parse(stringFromFile, game, verifier, owners); 
			if (gameObject != null) break;
		}
		return gameObject; 
	}
}
