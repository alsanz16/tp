package tp.p1.game;

import tp.p1.exceptions.CommandExecuteException;
import tp.p1.game.info.MovementDirection;

/**
 * Interfaz de control del jugador. Al menos una clase debe implementarla.
 *
 */
public interface IPlayerController {
	public void buySuperMissile() throws CommandExecuteException;
	public boolean move(MovementDirection movementDirection) throws CommandExecuteException;
	public void receivePoints(int points);
	public void setShockwave(boolean b);;
	public void shockWave() throws CommandExecuteException;;
	public void shootLaser(boolean b) throws CommandExecuteException;
}
