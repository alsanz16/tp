package tp.p1.game;

import tp.p1.game.info.PrinterType;

/**
 * Familia de clases de los impresores.
 *
 */
public abstract class GamePrinter {
	protected PrinterType printingType;
	public abstract String toString(Game game);
}
