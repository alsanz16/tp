package tp.p1.exceptions;

/**
 * Excepción que se lanzará cuando no se pueda ejecutar el movimiento de la nave.
 *
 */
public class OffWorldException extends CommandExecuteException {
	/**
	 * @param message Mensaje de error que se mostará al usuario
	 */
	public OffWorldException(String message) {
		super(message);
	}
}
