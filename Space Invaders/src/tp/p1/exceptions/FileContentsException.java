package tp.p1.exceptions;

/**
 * Excepción lanzada al tratar de cargar un juego desde un archivo no válido
 *
 */
public class FileContentsException extends CommandExecuteException {
	/**
	 * @param message Mensaje de error que se mostrará al usuario
	 */
	public FileContentsException(String message) {
		super(message);
	}

}
