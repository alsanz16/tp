package tp.p1.exceptions;

/**
 * Excepción lanzada al tratar de lanzar un misil habiendo uno en tablero
 *
 */
public class MissileInFlightException extends CommandExecuteException {
	/**
	 * @param message Mensaje de error que se mostrará al usuario
	 */
	public MissileInFlightException(String message) {
		super(message);
	}
}
