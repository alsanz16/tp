package tp.p1.exceptions;

import tp.p1.game.info.MovementDirection;

/**
 * Excepción lanzada al tratar de leer como dirección una string que
 * no se corresponda con una dirección válida. Las direcciones válidas
 * se encuentran en {@link MovementDirection}

 *
 */
public class DirectionFormatException extends CommandParseException {
	/**
	 * @param message Mensaje de error que se mostrará al usuario
	 */
	public DirectionFormatException(String message) {
		super("The command couldn't be parsed properly");
	}

}
