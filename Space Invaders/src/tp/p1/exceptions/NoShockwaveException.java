package tp.p1.exceptions;

import tp.p1.naves.proyectiles.Shockwave;

/**
 * Excepción que se lanzará al tratar de usar una {@link Shockwave} sin estar disponible.
 * *
 */
public class NoShockwaveException extends CommandExecuteException {
	/**
	 * @param message Mensaje de error que se mostrará al usuario
	 */
	public NoShockwaveException(String message) {
		super(message);
	}
}
