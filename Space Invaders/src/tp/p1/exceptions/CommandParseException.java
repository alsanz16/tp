package tp.p1.exceptions;

/**
 * Familia de excepciones por errores al leer un comando.
 *
 */
public class CommandParseException extends RuntimeException {
	/**
	 * @param message Mensaje de error que se mostrará al usuario
	 */
	public CommandParseException(String message) {
		super(message);
	}

}
