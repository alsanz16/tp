package tp.p1.exceptions;

import tp.p1.util.GameDifficulty;

/**
 * Excepción lanzada al tratar de leer como dificultad una cadena inválida. 
 * Las dificultades válidas se encuentran en {@link GameDifficulty}
 *  
 */
public class DifficultyFormatException extends CommandParseException {
	/**
	 * @param message Mensaje de error que se mostrará al usuario
	 */
	public DifficultyFormatException(String message) {
		super("The command couldn't be parsed properly");
	}

}
