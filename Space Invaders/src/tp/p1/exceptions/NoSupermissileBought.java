package tp.p1.exceptions;

import tp.p1.naves.proyectiles.SuperMissile;

/**
 * Excepción que se lanzará al tratar de lanzar {@link SuperMissile} sin haberlo comprado. 
 *
 */
public class NoSupermissileBought extends CommandExecuteException {
	/**
	 * @param message Mensaje de error que se mostrará al usuario
	 */
	public NoSupermissileBought(String message) {
		super(message);
	}
}
