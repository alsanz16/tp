package tp.p1.exceptions;

/**
 * Excepción que se lanzará al tratar de comprar un supermisil careciendo de puntos suficientes.
 * 
 */
public class NoPointsEnoughException extends CommandExecuteException {
	/**
	 * @param message Mensaje de error que se mostrará al usuario
	 */
	public NoPointsEnoughException(String message) {
		super(message);
	}
}
