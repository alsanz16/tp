package tp.p1.exceptions;

/**
 * Familia de excepciones por errores al ejecutar un comando.
 * 
 *
 */
public abstract class CommandExecuteException extends RuntimeException {
	
	/**
	 * @param message Mensaje de error a mostrar al usuario
	 */
	public CommandExecuteException(String message) {
		super(message);
	}
}
