package tp.p1.naves;

import java.util.Map;

import tp.p1.exceptions.DirectionFormatException;
import tp.p1.game.FileVerifier;
import tp.p1.game.Game;
import tp.p1.game.info.MovementDirection;
import tp.p1.game.info.ShipType;
import tp.p1.naves.proyectiles.BaseProjectile;
import tp.p1.naves.proyectiles.Bomb;
import tp.p1.util.Location;

public class DestroyerShip extends AlienShip {

	public DestroyerShip() {
		this(null, new Location(0,0));
	}
	
	public DestroyerShip(Game game, Location initialPosition) {
		super(game, ShipType.DESTROYER, initialPosition);
	}

	public DestroyerShip parse(String stringFromFile, Game game, FileVerifier verifier, 
			Map<Integer, BaseShip> owners) {
		String attributes[] = stringFromFile.split(";");
		if (!verifier.verifyAlienShipString(stringFromFile, game, type.getHp()) ||
				!attributes[0].contentEquals(type.getMapSymbol())) 
			return null;
		
		DestroyerShip newShip = new DestroyerShip(game , new Location(attributes[1]));
		newShip.hp = Integer.parseInt(attributes[2]);
		owners.put(Integer.parseInt(attributes[3]), newShip);
		
		try { game.getObjects().setMovementDirection(MovementDirection.parseDirection(attributes[4]));
		} catch (DirectionFormatException e) { e.printStackTrace(); }

		return newShip;

	} 
	
	@Override
	public void passTurn(boolean timeToMove) {
		if(game.randomChance(game.getDifficulty().getShootFrequency())) {
			BaseProjectile newProjectile = shoot();
			if (newProjectile != null) game.addObject(newProjectile);
		}
		super.passTurn(timeToMove);
	}	
	
	public Bomb shoot() {
		BaseProjectile newProjectile = null;
		if (projectile == null) projectile = newProjectile = new Bomb(game, position, this);
		return (Bomb) newProjectile;
	}
}
