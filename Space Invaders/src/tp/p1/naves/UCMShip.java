package tp.p1.naves;

import java.util.Map;

import tp.p1.game.FileVerifier;
import tp.p1.game.Game;
import tp.p1.game.info.PrinterType;
import tp.p1.game.info.ShipType;
import tp.p1.naves.proyectiles.BaseProjectile;
import tp.p1.naves.proyectiles.Missile;
import tp.p1.naves.proyectiles.SuperMissile;
import tp.p1.util.GameEvent;
import tp.p1.util.GameEventList;
import tp.p1.util.Location;

public class UCMShip extends BaseShip {
	private static ShipType type = ShipType.UCM;
	private int superMissileNumber = 0;
	
	private Boolean shockwave;

	public UCMShip() {
		this(null);
	}

	public UCMShip(Game game) {
		super (game, type);
	}

	public UCMShip(Game game, Location position) {
		super (game, type, position);
	}
	
	public void addSuperMissile(int superMissileNumber) {
		this.superMissileNumber += superMissileNumber;
	}
	
	public Boolean getShockwave() {
		return shockwave;
	}
	
	public int getSuperMissileNumber() {
		return superMissileNumber;
	}
		
	public UCMShip parse(String stringFromFile, Game game, FileVerifier verifier, Map<Integer, BaseShip> owners) {
		String attributes[] = stringFromFile.split(";");
		if (!verifier.verifyPlayerString(stringFromFile, game, type.getHp()) ||
				!attributes[0].contentEquals(type.getMapSymbol())) return null;
		
		UCMShip newShip = new UCMShip(game, new Location(attributes[1]));
		newShip.hp = Integer.parseInt(attributes[2]);
		owners.put(Integer.parseInt(attributes[3]), newShip);
		game.receivePoints(Integer.parseInt(attributes[4]));
		newShip.setShockwave(Boolean.parseBoolean(attributes[5]));
		newShip.addSuperMissile(Integer.parseInt(attributes[6]));
		return newShip;
	}
	
	@Override
	public void passTurn(boolean timeToMove) {
		;
	}

	public Boolean receiveBombAttack(Integer damage) {
		alterHP(-1*damage);
		return true;
	}
	
	public void setShockwave(Boolean status) {
		shockwave = status;
	}
	
	public boolean shoot(boolean superMissile) {
		BaseProjectile newProjectile = null;
		if (projectile == null) {
			if (superMissile) {
				projectile = newProjectile = new SuperMissile(game, position,this);
				superMissileNumber--;
			}
			else projectile = newProjectile = new Missile(game,position,this);
			game.addObject(newProjectile);
		}
		return newProjectile != null;
	}
	
	@Override
	public String toString () {
		if (getHp() <= 0) return "!xx!";
		else return super.toString();
	}

	public String toString(PrinterType printingType) {
		String retorno;
		if(printingType == PrinterType.BOARDPRINTER) 
			if (getHp() <= 0) retorno = "!xx!";
			else retorno = super.toString(printingType);
		else retorno = super.toString(printingType) + 
				String.format(";%d;%b;%d", game.getGameInfo().get("points"), 
						shockwave, superMissileNumber );
		return retorno;
	}
	
	@Override
	public GameEventList update(GameEventList events) {
		if (!isValid()) events.add(GameEvent.PLAYER_KILLED);
		return events;
	}
}