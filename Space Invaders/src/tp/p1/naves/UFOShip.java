package tp.p1.naves;

import java.util.Map;

import tp.p1.game.FileVerifier;
import tp.p1.game.Game;
import tp.p1.game.info.MovementDirection;
import tp.p1.game.info.ShipType;
import tp.p1.util.GameEvent;
import tp.p1.util.GameEventList;
import tp.p1.util.Location;

public class UFOShip extends BaseShip {
	public UFOShip() {
		this(null);
	}
	
	public UFOShip(Game game) {
		super(game, ShipType.OVNI);
	}
	
	public void move() {
		if (!super.move(MovementDirection.LEFT)) 
			isValid = false;
	}
	
	public UFOShip parse(String stringFromFile, Game game, FileVerifier verifier, Map<Integer, BaseShip> owners) {
		String attributes[] = stringFromFile.split(";");
		if (!verifier.verifyOvniString(stringFromFile, game, type.getHp()) ||
				!attributes[0].contentEquals(type.getMapSymbol())) 
			return null;
		
		this.game = game;
		this.position = new Location(attributes[1]);
		this.hp = Integer.parseInt(attributes[2]);
		return this;
	}
	
	@Override
	public void passTurn(boolean timeToMove) {
		move();
	}
	
	public Boolean receiveMissileAttack(Integer damage) {
		alterHP(-1*damage);
		return true;
	}

	public Boolean receiveShockWaveAttack(Integer damage) {
		alterHP(-1*damage);
		return true;
	}
	
	@Override
	public GameEventList update() {
		GameEventList newEvents = super.update();
		if (!isValid()) newEvents.add(GameEvent.OVNI_DESTROYED);
		return newEvents;
	}
	
	@Override
	public GameEventList update(GameEventList events) {
		if (!isValid)
			if (hp <= 0) events.add(GameEvent.OVNI_DESTROYED);
			else events.add(GameEvent.OVNI_OUT_OF_SCREEN);
		return events;
	}
}
