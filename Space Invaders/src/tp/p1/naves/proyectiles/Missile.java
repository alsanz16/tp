package tp.p1.naves.proyectiles;

import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import tp.p1.game.FileVerifier;
import tp.p1.game.Game;
import tp.p1.game.GameObject;
import tp.p1.game.info.ProjectileType;
import tp.p1.naves.BaseShip;
import tp.p1.util.Location;

public class Missile extends BaseProjectile {
	public Missile() {
		this(null, new Location(0,0), null);
	}
	
	public Missile(Game game, Location initialPos, BaseShip owner) {
		super(game, initialPos, owner, ProjectileType.MISSILE);
	}

	public Missile(Game game, Location initialPos, BaseShip owner, boolean superMissile) {
		super(game, initialPos, owner, (superMissile ? ProjectileType.SUPERMISSILE : ProjectileType.MISSILE));
	}
	
	protected BiFunction<GameObject, Integer, Boolean> getDmgFunction() {
		return (GameObject object, Integer dmg) -> object.receiveMissileAttack(dmg);
	}
	
	protected Function<GameObject, Boolean> getFilterFunction() {
		return (GameObject object) -> object.getPosition().equals(position);
	}
	
	public Missile parse(String stringFromFile, Game game, FileVerifier verifier, 
			Map<Integer, BaseShip> owners) {
		String attributes[] = stringFromFile.split(";");
		if (!verifier.verifyWeaponString(stringFromFile, game)
				|| !attributes[0].contentEquals(type.getMapSymbol())) 
			return null;
		
		BaseShip owner = owners.get(Integer.parseInt(attributes[2]));
		Missile newMissile = new Missile(game, new Location(attributes[1]), owner);
		owner.setProjectile(newMissile);
		return newMissile;	
		}
	
	public Boolean receiveBombAttack(Integer damage) {
		isValid = false;
		informOwner();
		return true;
	}
}
