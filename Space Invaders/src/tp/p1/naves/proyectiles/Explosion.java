package tp.p1.naves.proyectiles;

import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import tp.p1.game.FileVerifier;
import tp.p1.game.Game;
import tp.p1.game.GameObject;
import tp.p1.game.info.ProjectileType;
import tp.p1.naves.BaseShip;

public class Explosion extends BaseProjectile {
	public Explosion(Game game, BaseShip owner) {
		super(game, owner.getPosition(), owner, ProjectileType.EXPLOSION);
		this.owner = owner;
	}

	protected BiFunction<GameObject, Integer, Boolean> getDmgFunction() {
		return (GameObject object, Integer dmg) -> object.receiveExplosionAttack(dmg);
	}
	
	protected Function<GameObject, Boolean> getFilterFunction() {
		return (GameObject object) -> {
			return Math.abs(object.getPosition().getX() - position.getX()) <= 1 &&
					Math.abs(object.getPosition().getY() - position.getY()) <= 1 ;
		};
	}
	
	public void informOwner() {
		;
	}
	
	public Explosion parse(String stringFromFile, Game game, FileVerifier verifier, Map<Integer, BaseShip> owners) {
		return null;
	}
	
	public void passTurn(boolean timeToMove) {		
		super.passTurn(timeToMove);
		isValid = false;
	}


}
