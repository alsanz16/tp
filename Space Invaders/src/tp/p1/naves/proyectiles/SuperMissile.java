package tp.p1.naves.proyectiles;

import tp.p1.game.Game;
import tp.p1.naves.BaseShip;
import tp.p1.util.Location;

public class SuperMissile extends Missile {
	public SuperMissile() {
		this(null, new Location(0,0), null);
	}
	
	public SuperMissile(Game game, Location initialPos, BaseShip owner) {
		super(game, initialPos, owner, true);
	}
}
