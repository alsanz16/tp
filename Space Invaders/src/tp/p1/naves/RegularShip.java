package tp.p1.naves;

import java.util.Map;

import tp.p1.game.FileVerifier;
import tp.p1.game.Game;
import tp.p1.game.info.MovementDirection;
import tp.p1.game.info.ShipType;
import tp.p1.naves.proyectiles.Explosion;
import tp.p1.util.Location;

public class RegularShip extends AlienShip {
	private boolean exploded = false;
	
	public RegularShip() {
		this(null, new Location(0,0));
	}
	
	public RegularShip(Game game, Location initialPosition) {
		super(game, ShipType.REGULAR, initialPosition);
	}
	
	protected void alterHP(int diff) {
		super.alterHP(diff);
		if (!isValid && !exploded && type == ShipType.EXPLOSIVE) {
			game.addObject(new Explosion(game, this));
			exploded = true;
		}
	}
	
	public RegularShip parse(String stringFromFile, Game game, FileVerifier verifier, Map<Integer, BaseShip> owners) {
		String attributes[] = stringFromFile.split(";");
		boolean isExplosive;
		if(!verifier.verifyAlienShipString(stringFromFile, game, type.getHp())
				|| !(isExplosive = attributes[0].contentEquals(type.EXPLOSIVE.getMapSymbol()))
				&& !attributes[0].contentEquals(type.REGULAR.getMapSymbol()))
				return null;

		RegularShip newShip = new RegularShip(game , new Location(attributes[1]));
		game.getObjects().setMovementDirection(MovementDirection.parseDirection(attributes[4]));
		
		newShip.type = isExplosive ? ShipType.EXPLOSIVE : ShipType.REGULAR;
		newShip.hp = Integer.parseInt(attributes[2]);
		return newShip;
	}
	
	@Override
	public void passTurn(boolean turnToMove) {
		if (game.randomChance(game.getDifficulty().getExplosiveTransformationFrequency()))
			type = ShipType.EXPLOSIVE;
		super.passTurn(turnToMove);
	}
}