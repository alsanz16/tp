package tp.p1.util;

import tp.p1.game.Game;

public class Location {
	private int x, y;

	public Location(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Location(String string) {
		// TODO Auto-generated constructor stub
		String coors[] = string.split(",");
		this.x = Integer.parseInt(coors[0]);
		this.y = Integer.parseInt(coors[1]);
	}

	public Location copy() {
		return new Location(this.x, this.y);
	}

	public Boolean equals(Location pos) {
		return x == pos.getX() && y == pos.getY();
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean isValid() {
		int dimX = Game.boardDimension.getX();
		int dimY = Game.boardDimension.getY();
		return 0 <= x && x < dimX && 0 <= y && y < dimY;
	}

	public void setEqual(Location position) {
		x = position.getX();
		y = position.getY();
	}

	public boolean setPosition(Location diff) {
		boolean successY = setY(diff.getY());
		boolean successX = setX(diff.getX());
		return successX && successY;
	}

	private boolean setX(int diffX) {
		boolean success = true;
		this.x += diffX;
		if (!this.isValid()) {
			this.x -= diffX;
			success = false;
		}
		return success;
	}

	private boolean setY(int diffY) {
		boolean success = true;
		this.y += diffY;
		if (!this.isValid()) {
			this.y -= diffY;
			success = false;
		}
		return success;
	}
}
