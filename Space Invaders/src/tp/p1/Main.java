package tp.p1;
// New comment
import tp.p1.exceptions.CommandParseException;
import tp.p1.game.Controller;
import tp.p1.util.GameDifficulty;

public class Main {

	/*
	 * TODO: lanzar supermisil tiene quelanzar excepción si no hay
	 * supermisiles 
	 *  TODO: mas excepciones y mas concretas
	 * 
	 */
	public static void main(String[] args) {
		Controller controller = null;
		GameDifficulty difficulty = null;
		try { 
			difficulty = GameDifficulty.parseDifficulty(args[0]);
			controller = new Controller(difficulty, Integer.parseInt(args[1]));
		} 
		catch (IndexOutOfBoundsException e) {
			if (args.length >= 1) controller = new Controller(difficulty);
			else throw new CommandParseException("");
		} catch (CommandParseException | NumberFormatException e) {
			System.out.println("Usage: Main <EASY|HARD|INSANE> [seed]: "
					+ "level must be one of: EASY, HARD, INSANE");
			System.exit(1);
		}
		controller.run();
	}

}
